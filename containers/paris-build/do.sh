#!/bin/bash
source /tmp/env
create_singularityfile
date_str="$(date +'%Y-%m-%d_%H-%M')"
build_singularitycontainer  $date_str
deploy_singularitycontainer $date_str romed@astro-particle.uibk.ac.at:/SCRATCH/.
deploy_testanalyses         $date_str romed@astro-particle.uibk.ac.at:/SCRATCH/.
run_testanalyses            $date_str romed@astro-particle.uibk.ac.at:/SCRATCH/.
